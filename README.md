Installation:

1. Upload the CSS files to the /template/css directory in your Banner Exchange folder.
2. Upload the Image files to the /template/images folder (if any, you may need to create this folder).
3. Access your Administrative Control Panel, then go to the Edit Style Sheets option.
4. Select the CSS file you uploaded from the list, and click Submit.